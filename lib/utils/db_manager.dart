import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import '../models/contractor.dart';

class DBProvider {
  DBProvider._();

  static final DBProvider db = DBProvider._();

  Future<Database> _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = getDatabaseInstance();
    return _database;
  }

  Future<Database> getDatabaseInstance() async {
    var databasePath = await getDatabasesPath();
    String path = join(databasePath, 'contractors_database.db');
    final Future<Database> database = openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute(
          "CREATE TABLE contractors(id INTEGER PRIMARY KEY, civility TEXT,"
          " firstname TEXT, lastname TEXT, address_1 TEXT, address_2 TEXT,"
          " postal_code TEXT, city TEXT, cell_phone TEXT, email TEXT)");
    });
    return database;
  }

  Future<void> insertContractor(Contractor contractor) async {
    // Get a reference to the database.
    final Database db = await database;

    // `conflictAlgorithm`. In this case, if data is inserted
    // multiple times, it replaces the previous data.
    await db.insert(
      'contractors',
      contractor.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  // A method that add new contractors from api to the contractors table.
  addAllContractors(List<Contractor> contractors) async {
    List list = await contractors;
    list.forEach((contractor) {
      insertContractor(contractor);
    });
  }

  Future<List<Contractor>> getAllContractors() async {
    final Database db = await database;

    final List<Map<String, dynamic>> maps = await db.query('contractors');

    // Convert the List<Map<String, dynamic> into a List<Contractor>.
    return List.generate(maps.length, (i) {
      return Contractor(
        civility: maps[i]['civility'],
        firstname: maps[i]['firstname'],
        lastname: maps[i]['lastname'],
        address_1: maps[i]['address_1'],
        address_2: maps[i]['address_2'],
        postal_code: maps[i]['postal_code'],
        city: maps[i]['city'],
        cell_phone: maps[i]['cell_phone'],
        email: maps[i]['email'],
      );
    });
  }

  Future<List<Contractor>> findContractors(String key) async {
    final Database db = await database;
    final maps = await db.rawQuery(
        "SELECT * FROM contractors WHERE firstname LIKE '%$key%%' OR lastname LIKE '%$key%%'");

    return List.generate(maps.length, (i) {
      return Contractor(
        civility: maps[i]['civility'],
        firstname: maps[i]['firstname'],
        lastname: maps[i]['lastname'],
        address_1: maps[i]['address_1'],
        address_2: maps[i]['address_2'],
        postal_code: maps[i]['postal_code'],
        city: maps[i]['city'],
        cell_phone: maps[i]['cell_phone'],
        email: maps[i]['email'],
      );
    });
  }

  Future<void> removeTable() async {
    final Database db = await database;
    await db.delete(
      "contractors",
    );
  }
}
