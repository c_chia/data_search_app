import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'constants.dart';
import '../models/contractor.dart';

class ContractorData {
  /**
   *  Get Contractors list from Api
   */
  Future<List<Contractor>> getContractorsFromApi() async {
    var uri = Uri.https(baseApi, suffixApi);

    Map<String, String> myHeaders = Map<String, String>();
    myHeaders[apiKey] = apiTokenValue;

    final response = await http.get(uri, headers: myHeaders);
    return parseContractors(response.body);
  }

  /**
   *  Parse response from Api to Contractors list
   */
  List<Contractor> parseContractors(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();

    return parsed.map<Contractor>((json) => Contractor.fromJson(json)).toList();
  }

  /**
   *  Post new Contractor
   */

  Future<Response> postContractorToApi() async {
    var uri = Uri.https(baseApi, suffixApi);

    Map<String, String> myHeaders = Map<String, String>();
    myHeaders[apiKey] = apiTokenValue;

    return await http.post(uri, headers: myHeaders);
  }
}
