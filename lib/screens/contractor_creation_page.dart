import 'dart:ui';
import 'package:data_search_app/models/contractor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import '../colors.dart';
import '../utils/db_manager.dart';
import 'package:data_search_app/components/text_styles.dart';

class ContractorCreationPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ContractorCreationPageState();
}

class ContractorCreationPageState extends State<ContractorCreationPage> {
  final _formKey = GlobalKey<FormState>();

  var civController = TextEditingController();
  var fnController = TextEditingController();
  var lnController = TextEditingController();
  var adController = TextEditingController();
  var adcController = TextEditingController();
  var pcController = TextEditingController();
  var cityController = TextEditingController();
  var phoneController = TextEditingController();
  var emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Scaffold(
          backgroundColor: kBgdColor,
          appBar: AppBar(
            title: AnimatedStyledText(),
            iconTheme: IconThemeData(
              color: kHighContrastColor,
            ),
          ),
          body: SingleChildScrollView(
              child: Column(children: <Widget>[
            Center(
                child: TextfieldItem(
                    labelTxt: "Civility", controller: civController)),
            Center(
                child: Row(children: <Widget>[
              Expanded(
                  child: TextfieldItem(
                      labelTxt: "Firstname", controller: fnController)),
              Expanded(
                  child: TextfieldItem(
                      labelTxt: "Lastname", controller: lnController))
            ])),
            Center(
                child: TextfieldItem(
                    labelTxt: "Address", controller: adController)),
            Center(
                child: TextfieldItem(
                    labelTxt: "Address Complement", controller: adcController)),
            Center(
                child: Row(children: <Widget>[
              Expanded(
                  child: TextfieldItem(
                      labelTxt: "Postal Code", controller: pcController)),
              Expanded(
                  child: TextfieldItem(
                      labelTxt: "City", controller: cityController))
            ])),
            Center(
                child: TextfieldItem(
                    labelTxt: "Phone Number", controller: phoneController)),
            Center(
                child: TextfieldItem(
                    labelTxt: "Email", controller: emailController)),
            SizedBox(height: 70),
          ])),
          floatingActionButton: Theme(
            data: ThemeData(accentColor: kAccentColor, fontFamily: 'Pacifico'),
            child: FloatingActionButton.extended(
                onPressed: () {
                  // Validate returns true if the form is valid, or false otherwise.
                  if (_formKey.currentState.validate()) {
                    // If the form is valid, display a snackbar. In the real world,
                    // you'd often call a server or save the information in a database.
                    saveContractor();
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                          backgroundColor: kAccentColor,
                          content: Text('Processing Data')),
                    );
                  }
                },
                icon: Icon(Icons.save),
                label: Text("Save"),
                foregroundColor: kContrastColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18))),
          ),
        ));
  }

  void saveContractor() async {
    final contractor = Contractor(
        civility: civController.text,
        firstname: fnController.text,
        lastname: lnController.text,
        address_1: adController.text,
        address_2: adcController.text,
        postal_code: pcController.text,
        city: cityController.text,
        cell_phone: phoneController.text,
        email: emailController.text);

    await DBProvider.db.insertContractor(contractor);
    setState(() {});
  }
}

class TextfieldItem extends StatelessWidget {
  TextfieldItem({@required this.labelTxt, @required this.controller});

  final String labelTxt;
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: TextFormField(
        cursorColor: kHighContrastColor,
        controller: this.controller,
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontFamily: 'Agne',
            color: kHighContrastColor),
        decoration: InputDecoration(
            border: OutlineInputBorder(),
            enabledBorder: const OutlineInputBorder(
              borderSide: const BorderSide(color: kContrastColor),
            ),
            labelText: labelTxt),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
      ),
    );
  }
}
