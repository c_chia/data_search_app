import 'package:data_search_app/components/contractor_card.dart';
import 'package:data_search_app/components/text_styles.dart';
import 'package:data_search_app/models/contractor.dart';

import '../colors.dart';
import '../utils/db_manager.dart';
import 'contractor_creation_page.dart';
import 'package:flutter/material.dart';

class SearchPage extends StatefulWidget {
  @override
  SearchPageState createState() => SearchPageState();
}

class SearchPageState extends State<SearchPage>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<Color> animation;

  static const assetsImage = AssetImage('images/bot_creator.png');
  static const botImage = Image(
    image: assetsImage,
  );

  @override
  void initState() {
    super.initState();

    controller =
        AnimationController(duration: Duration(seconds: 3), vsync: this);

    animation =
        ColorTween(begin: kAccentColor, end: kBgdColor).animate(controller);

    controller.forward();
    controller.addListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  void _addItem() {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => ContractorCreationPage()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: animation.value,
      floatingActionButton: Theme(
        data: ThemeData(accentColor: kAccentColor),
        child: FloatingActionButton(
            onPressed: _addItem,
            tooltip: 'Add',
            child: Icon(Icons.add),
            foregroundColor: kContrastColor,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18))),
      ),
      appBar: AppBar(
          iconTheme: IconThemeData(
            color: kHighContrastColor,
          ),
          actions: <Widget>[
            IconButton(
              onPressed: () {
                showSearch(
                  context: context,
                  delegate: Search(),
                );
              },
              icon: Icon(Icons.search),
            )
          ],
          centerTitle: true,
          title: PacificoStyledText()),
      body: Center(child: botImage),
    );
  }
}

class Search extends SearchDelegate {
  List<Contractor> dbContractorsList;
  Contractor selectedResult;

  //Search();

  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[];
  }

  @override
  Widget buildLeading(BuildContext context) {
    //Display back home bar
    return IconButton(
      icon: Icon(
        Icons.arrow_back,
        color: kHighContrastColor,
      ),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    // Display contractor selected details
    return ContractorCard(selectedResult);
  }

  Future<List<Contractor>> getDbContractorList(String query) async {
    return await DBProvider.db.findContractors(query);
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // Build suggestion list : items matching with the query string

    return Scaffold(
        body: FutureBuilder(
            future: getDbContractorList(query),
            builder: (context, suggestionList) {
              if (suggestionList.connectionState == ConnectionState.done &&
                  suggestionList.hasData) {
                return ListView.builder(
                    itemCount: suggestionList.data.length,
                    itemBuilder: (context, index) {
                      return ListTile(
                          title: Text(
                              suggestionList.data[index].firstname +
                                  ' ' +
                                  suggestionList.data[index].lastname,
                              style: bodyTextStyle()),
                          onTap: () {
                            // On tap displays details of the element selected from showResults calling buildResult
                            selectedResult = suggestionList.data[index];
                            showResults(context);
                          });
                    });
              } else {
                return Center(child: CircularProgressIndicator());
              }
            }));
  }
}
