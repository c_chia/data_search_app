import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFFFF9748);
const kBgdColor = Color(0xFFFFB648);
const kAccentColor = Color(0xFF71EDCE);

const kContrastColor = Color(0xFFFF7348);
const kHighContrastColor = Color(0xFFFF5725);
