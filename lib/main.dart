import 'utils/api_data_provider.dart';
import 'package:get_it/get_it.dart';
import 'components/contractor_card.dart';
import 'colors.dart';
import 'package:flutter/material.dart';
import 'utils/db_manager.dart';
import 'models/contractor.dart';
import 'screens/search_page.dart';

void main() {
  setupLocator();
  runApp(DataSearchApp());
}

class DataSearchApp extends StatefulWidget {
  DataSearchApp({Key key}) : super(key: key);

  @override
  DataSearchAppState createState() => DataSearchAppState();
}

class DataSearchAppState extends State<DataSearchApp> {
  Future<List<Contractor>> contractors;

  @override
  void initState() {
    super.initState();
    var contractorData = ContractorData();
    List<Contractor> contractors;
    contractorData.getContractorsFromApi().then((value) {
      setState(() {
        contractors = value;
        dbSync(contractors);
      });
    });
  }

  void dbSync(List<Contractor> contractors) async {
    await DBProvider.db.removeTable();
    await DBProvider.db.addAllContractors(contractors);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark().copyWith(
        primaryColor: kPrimaryColor,
        accentColor: kHighContrastColor,
      ),
      home: SearchPage(),
    );
  }
}

GetIt locator = GetIt();

void setupLocator() {
  locator.registerSingleton(CallsAndMessagesService());
}
