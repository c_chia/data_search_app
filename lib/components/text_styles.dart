import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';

import '../colors.dart';

Text PacificoStyledText() {
  return Text('Search Contractor',
      style: TextStyle(
          fontSize: 25.0, color: kHighContrastColor, fontFamily: 'Pacifico'));
}

AnimatedTextKit AnimatedStyledText() {
  return AnimatedTextKit(animatedTexts: [
    TypewriterAnimatedText('Contractor Creation',
        textStyle: const TextStyle(
          fontSize: 25.0,
          color: kHighContrastColor,
          fontFamily: 'Pacifico',
        ))
  ]);
}

TextStyle bodyTextStyle() {
  return TextStyle(fontFamily: 'Agne');
}
