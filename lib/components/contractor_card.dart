import 'package:data_search_app/colors.dart';
import 'package:data_search_app/components/text_styles.dart';
import 'package:data_search_app/models/contractor.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../main.dart';

class ContractorCard extends StatelessWidget {
  final CallsAndMessagesService _service = locator<CallsAndMessagesService>();
  Contractor contractor;
  ContractorCard(Contractor contractor) {
    this.contractor = contractor;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBgdColor,
      body: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                nameContractor(),
                style: TextStyle(
                    fontFamily: 'Pacifico',
                    fontSize: 42.0,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
              Center(
                  child: Text(
                addressContractor(),
                style: TextStyle(
                  fontFamily: 'Source Sans Pro',
                  color: Colors.amber.shade50,
                  fontSize: 20.0,
                  letterSpacing: 1.5,
                  fontWeight: FontWeight.bold,
                ),
              )),
              SizedBox(
                height: 42.0,
                width: 150.0,
                child: Divider(
                  color: Colors.amber.shade50,
                ),
              ),
              Card(
                  color: Colors.white,
                  margin:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                  child: ListTile(
                    leading: Icon(
                      Icons.phone,
                      color: kHighContrastColor,
                    ),
                    title: Text(
                      phoneNbContractor(),
                      overflow: TextOverflow.fade,
                      softWrap: false,
                      style: TextStyle(
                          color: kContrastColor,
                          fontFamily: 'Source Sans Pro',
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold),
                    ),
                    onTap: () {
                      _service.call(phoneNbContractor());
                    },
                  )),
              Card(
                  color: Colors.white,
                  margin:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                  child: ListTile(
                    leading: Icon(
                      Icons.email,
                      color: kHighContrastColor,
                    ),
                    title: Text(
                      emailContractor(),
                      overflow: TextOverflow.fade,
                      softWrap: false,
                      style: TextStyle(
                          fontSize: 17.0,
                          color: kContrastColor,
                          fontFamily: 'Source Sans Pro',
                          fontWeight: FontWeight.bold),
                    ),
                    onTap: () {
                      _service.sendEmail(emailContractor());
                    },
                  ))
            ],
          )),
    );
  }

  String nameContractor() {
    return contractor.civility +
        "\n" +
        contractor.lastname +
        " " +
        contractor.firstname;
  }

  String addressContractor() {
    return contractor.address_1 +
        " " +
        contractor.address_2 +
        "\n" +
        contractor.postal_code +
        " " +
        contractor.city;
  }

  String phoneNbContractor() {
    return contractor.cell_phone;
  }

  String emailContractor() {
    return contractor.email;
  }
}

class CallsAndMessagesService {
  void call(String number) => launch("tel:$number");
  void sendSms(String number) => launch("sms:$number");
  void sendEmail(String email) => launch("mailto:$email");
}
