class Contractor {
  int id;
  final String civility;
  final String firstname;
  final String lastname;
  final String address_1;
  final String address_2;
  final String postal_code;
  final String city;
  final String cell_phone;
  final String email;

  Contractor(
      {this.civility,
      this.firstname,
      this.lastname,
      this.address_1,
      this.address_2,
      this.postal_code,
      this.city,
      this.cell_phone,
      this.email});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'civility': civility,
      'firstname': firstname,
      'lastname': lastname,
      'address_1': address_1,
      'address_2': address_2,
      'postal_code': postal_code,
      'city': city,
      'cell_phone': cell_phone,
      'email': email
    };
  }

  factory Contractor.fromJson(Map<String, dynamic> json) {
    return Contractor(
      civility: json['civility'],
      firstname: json['firstname'],
      lastname: json['lastname'],
      address_1: json['address_1'],
      address_2: json['address_2'],
      postal_code: json['postal_code'],
      city: json['city'],
      cell_phone: json['cell_phone'],
      email: json['email'],
    );
  }
}
